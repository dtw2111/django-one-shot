from django.urls import path

from todos.views import show_todo_lists, show_todo_list_detail

urlpatterns = [
    path(
        "",
        show_todo_lists,
        name="todo_list_list",
    ),
    path(
        "<int:pk>/",
        show_todo_list_detail,
        name="todo_list_detail",
    ),
]
