from django.shortcuts import render
from todos.models import TodoList

# from django.views.generic.detail import DetailView

# Create your views here.


def show_todo_lists(request):
    todolists = TodoList.objects.all()
    context = {"todo_lists": todolists}
    return render(request, "todos/list.html", context)


def show_todo_list_detail(request, pk):
    todo_list = TodoList.objects.get(pk=pk)
    context = {"todo_list": todo_list}
    return render(request, "todos/detail.html", context)
